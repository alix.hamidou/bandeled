//Alix Hamidou
//Rebond avec fonction mathematique
//11/03/2021

#include <Arduino.h>
#define FASTLED_ESP8266_RAW_PIN_ORDER
#include <FastLED.h>
#include <math.h>

#define NUM_LEDS 10 //nombre de LED du ruban
#define DATA_PIN D8 //D4 broche du bus de commande du ruban

//Création d'un tableau pour stocker et contrôler les LEDS
CRGB leds[NUM_LEDS];
//Velocité de rebond calculer par la fonction
int velocity = NUM_LEDS;
//Etat actuel (nombre de led allumé)
int state = 0;
//Sens de déplacement
int way = 1;
//Nombre de rebond
int x = 0;

void setup() {
  //configuration du ruban de LEDs
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  Serial.begin(9600);
}

int rebond(int prmX){
  return ((NUM_LEDS)*pow((2.0/3.0),prmX));
}

void loop() { 
  Serial.println(rebond(x));
  //On efface tout
  FastLED.clear();
  FastLED.show();

  //Pour chaque led
  for(int i=0;i<NUM_LEDS;i++){
    //On affiche le nombre de led de l'etat
    if(i<state){
      leds[i] = CHSV((i*255)/(NUM_LEDS-1),187,255);
    }
  }
  
  //Etat suivant
  state+=way;
  //Si on arrive au bout
  if(state == velocity){
    //On change de sens
    way=-1;
  }
  //Quand tout es eteint
  if(state==0){
    //On repasse en marche avant
    way=1;
    //On baisse la vélocité
    velocity = rebond(x);
    //Rebond suivant
    x++;
  }
  //Si la vélocité est a 0
  if(velocity==0){
    //On remet le bon sens
    way=1;
    //On remet au max
    velocity = NUM_LEDS;
    //On remet le nombre de rebond a 0
    x = 0;
  }

  //On affiche
  FastLED.show();
  delay(50);
}