//Alix Hamidou
//Chenillard unidirectionnel
//10/03/2021

#include <Arduino.h>
#define FASTLED_ESP8266_RAW_PIN_ORDER
#include <FastLED.h>

#define NUM_LEDS 10 //nombre de LED du ruban
#define DATA_PIN D8 //D4 broche du bus de commande du ruban

//Création d'un tableau pour stocker et contrôler les LEDS
CRGB leds[NUM_LEDS];

//Led actuel 
int ledOn = 0;

void setup() {
  //configuration du ruban de LEDs
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
}

void loop() {
  //On efface tout
  FastLED.clear();
  FastLED.show();

  //On definie la couleur de la led actuel selon sa position sur la bande
  leds[ledOn] = CRGB::Red;

  //On passe a la led suivante
  ledOn++;
  //Si on a atteind la limite de la bande
  if(ledOn==NUM_LEDS){
    //On retourne au debut
    ledOn = 0;
  }
  //On affiche
  FastLED.show();
  delay(100);
}