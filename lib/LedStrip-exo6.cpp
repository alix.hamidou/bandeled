//Alix Hamidou
//Chenillard unidirectionnel multicolore
//10/03/2021

#include <Arduino.h>
#define FASTLED_ESP8266_RAW_PIN_ORDER
#include <FastLED.h>

#define NUM_LEDS 10 //nombre de LED du ruban
#define DATA_PIN D8 //D4 broche du bus de commande du ruban

//Création d'un tableau pour stocker et contrôler les LEDS
CRGB leds[NUM_LEDS];
//HUE de base
int baseHUE = 0;

void setup() {
  //configuration du ruban de LEDs
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
}

void allumerLeds(int hueBase){
  //Pour chaque led
  for(int i=0;i<NUM_LEDS;i++){
    //On allume la led en fonction du hue
    leds[i] = CHSV((hueBase+i*10)%255,187,255);
  }
  //On affiche
  FastLED.show();
}

void loop() {
  //On allume les leds
  allumerLeds(baseHUE);

  //HUE suivant limité a 255
  baseHUE = (baseHUE+1)%255;
}