//Alix Hamidou
//Chenillard unidirectionnel multicolore
//10/03/2021

#include <Arduino.h>
#define FASTLED_ESP8266_RAW_PIN_ORDER
#include <FastLED.h>

#define NUM_LEDS 10 //nombre de LED du ruban
#define DATA_PIN D8 //D4 broche du bus de commande du ruban

//Création d'un tableau pour stocker et contrôler les LEDS
CRGB leds[NUM_LEDS];
//Nombre led allumer a partir du centre
int ledsState = 0;
//Sens 
int way = 1;

void setup() {
  //configuration du ruban de LEDs
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
}

void loop() { 
  //On efface tout
  FastLED.clear();
  FastLED.show();

  //Pour chaque led en partant du centre
  for(int i=0;i<NUM_LEDS;i++){
    //Si la led entre dans les leds a allumer dans cette etat
    if((i > (NUM_LEDS/2)-ledsState && i<NUM_LEDS/2) || (i > (NUM_LEDS/2)-1 && i < (NUM_LEDS/2)+ledsState)){
      //On definie la couleur de la led actuel selon sa position sur la bande
      leds[i] = CHSV((i*255)/(NUM_LEDS-1),187,255);
    }
  }

  ledsState+=way;
  //Si on a atteind une limite de la bande
  if(ledsState > NUM_LEDS/2 || ledsState==0){
    way=way*-1;
  }

  //On affiche
  FastLED.show();
  delay(100);
}