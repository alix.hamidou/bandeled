//Alix Hamidou
//Chenillard unidirectionnel avec 3 LEDS actives
//10/03/2021

#include <Arduino.h>
#define FASTLED_ESP8266_RAW_PIN_ORDER
#include <FastLED.h>

#define NUM_LEDS 10 //nombre de LED du ruban
#define DATA_PIN D8 //D4 broche du bus de commande du ruban

//Création d'un tableau pour stocker et contrôler les LEDS
CRGB leds[NUM_LEDS];

//Led actuel (à NUM_LEDS car pour la led précédante, il passera a NUM_LEDS-1 plutot que -1)
int ledOn = NUM_LEDS;

void setup() {
  //configuration du ruban de LEDs
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
}

void allumerLeds(int prmNumPrincipal){
  //On efface tout
  FastLED.clear();
  FastLED.show();

  //On definie la couleur de la led actuel selon sa position sur la bande (principale)
  leds[ledOn%10] = CHSV(150,187,255);
  //Led juste devant
  leds[(ledOn+1)%10] = CHSV(150,187,127);
  //Led juste deriere
  leds[(ledOn-1)%10] = CHSV(150,187,127);


  //On affiche
  FastLED.show();
}

void loop() {
  //On allume les leds
  allumerLeds(ledOn);

  //On passe a la led suivante
  ledOn++;
  //Si on a atteind une limite de la bande
  if(ledOn==NUM_LEDS*2){
    //On retourne au debut (position NUM_LEDS)
    ledOn = NUM_LEDS;
  }
  delay(100);
}